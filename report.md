﻿\newpage

# Introduction

Bonjour, moi c'est Maël, comme sur la page de garde de ce document. 
Je pose déjà ça là au cas où, si vous voulez me contacter, j'ai le même nom sur
Facebook : Maël Le Garrec (hatrix). Et par mail : mael at legarrec.org.

Je suis parti durant l'été 2016 sur un petit coup de tête en stop. Mon but
principal était de rejoindre la Pologne, en partant de Paris.

J'écris ce document plusieurs mois après mon voyage. Je peux cependant compter
sur mon fidèle allié le bloc-notes. Celui-ci me quitte rarement, il a été ici
d'une aide très précieuse, c'est fou le nombre de trucs intéressants (ou non)
qu'on peut noter. Pour le coup, énormément de notes sur ce que j'ai fait de mes
journées pour éviter de les oublier. C'est grace à lui que vous avez les yeux
posés sur ce document, remerciez le s'il vous plaît.

\sout{Ceci, est mon histoire.} Je vais vous raconter ici pourquoi je suis
parti, quelles étaient mes motivations, avec quel matos, comment je dormais, 
comment je mangeais, etc.

Je vais ensuite vous  raconter mon voyage, jour après jour ; même qu'il y aura
des photos pour éviter de s'endormir !
On continuera ensemble vers des astuces génériques et autres explications 
concernant le stop, pratiqué en France mais surtout en Europe.

J'ai remarqué en parlant de ce voyage qu'énormément d'aspects peuvent être 
flous, on est là pour éclaircir tout ça en s'amusant. Ça fait très mots croisés
en cours d'allemand dit comme ça, mais croyez moi, ça va être fun.

Allez viens, prends ma main, on part à l'aventure !

\newpage

# Questions fréquentes

## Pourquoi partir

Alors pourquoi partir, ça c'est souvent une question sur laquelle je sèche. 
Quand on décide d'entreprendre un tel voyage, on sait jamais dans quoi on 
s'embarque. Se dire avant de partir « je pars pour ça », c'est pas vraiment une
bonne idée. Du moins, faut rester vague pour rester ouvert.

Moi je suis parti parce que je m'emmerdais, clairement. J'avais du temps à 
tuer, j'avais envie de retourner en Pologne et de visiter un peu l'Europe en 
même temps. Alors oui c'est sûr, ça ce sont des raisons plutôts intéressantes
mais on peut aussi le faire en train ou en voiture.

À ça on rajoute le manque d'argent ? Allez, c'est parti pour du stop… Par
contre il faut différencier deux choses : respecter un peu les gens ou être un
fils de pute. Alors oui on part sans argent, mais ça veut pas dire qu'on va 
faire le clochard durant tout le voyage.
Les mecs qui mendient dans les gares ou sur les places pour se payer un billet
de train ou d'avion, voire même pour bouffer, vous allez vous faire foutre. La
liberté dans le voyage et en stop ça a jamais été obtenu comme ça, c'est juste
nuire à l'image des autres. Si vous partez du principe que votre voyage sera
gratuit parce que vous vous reposez sur les autres, vous êtes un gros connard.

C'est aussi ça que j'ai recherché du coup : un challenge. J'étais prêt à 
galérer pour deux choses principalement : dormir et bouger. Pour tout le reste,
il y a Mastercard.

Le plus important je pense c'est la connaissance de soi qu'on a à la fin. Oui
ça fait hippie de base qui dit avoir fait l'école de la vie, n'empêche qu'à la
fin on est beaucoup plus sociable. Quand on est livré à soi même dans un pays
où personne parle sa langue, faut se sortir les doigts du cul.

Du coup après avoir gueulé un peu, on se rend compte qu'on peut partir pour 
plein de raisons. Même si vous n'avez pas les mêmes que moi, faites ce que vous
voulez, j'suis pas votre mère.

\newpage

## Quand

Quand partir, on s'en fout pas mal en vrai. Si la destination que vous visez
vous intéresse aussi bien en été qu'en hiver, eh bien allez y en été. Ça fait
toujours moins de conneries à porter.

Ce qui est plus important, c'est combien de temps on veut partir. J'avais en 
tête environ deux semaines, ça a duré 2 semaines et demies. Faut pas être 
pressé, c'est pas le but de toute façon…

Ensuite évitez les guerres et autres conneries, ça va de soi.

## Avec qui

Je suis parti en solo, ça permet de faire vraiment ce qu'on veut. Ça oblige à 
sociabiliser avec n'importe qui.

Alors forcément au début c'est galère, on se balade dans la ville un peu au pif
sans vraiment regarder partout. Puis avec le temps on se faufile dans toutes
les petites rues, on parle aux touristes pour avoir quelques infos.

Après ça empêche pas de partir avec votre meilleur ami, votre partenaire ou que
sais-je. Faut juste vous entendre suffisamment pour pas se taper dessus à 
chaque fois que l'un prend une mauvaise décision.
Le nombre de fois où j'ai fait des conneries en solo, que ça soit de planter la 
tente sous une éolienne, me faire attaquer par des oies ou voyager dans un 
gofast, eh bien faut prendre sur soi.

Autant je vois quelques personnes avec qui j'aimerais bien voyager, mais je 
sais pas si ces gens là voudraient de moi dans ces conditions !

\newpage

## Quel matériel

Salut les filles, aujourd'hui un petit what's in my bag. Avant toute chose, 
quelques pré-requis : un dos. Parce que oui, marcher des jours avec un sac à 
dos, ça se sent. Le matos là c'est très subjectif, emportez ce que vous voulez,
je détaille juste ce que j'ai utilisé et ce que j'aurais aimé avoir.

On commence par la base : un fucking sac à dos. Il vous en faut un ni trop 
petit, ni trop grand. J'ai tapé dans le 65L, y'a carrément assez de place pour 
mettre tout ce qu'on veut dedans. Tellement que le sac de couchage et le 
matelas étaient à l'intérieur, alors que ça peut se foutre à l'extérieur. On
peut prendre plus petit sans souci, tant que ça tient bien et que c'est 
confortable.

![Sac par rapport à un Eastpak de base](images/sac2.jpg)

Plus bas, quelques catégories pour mieux s'y retrouver. Ça veut pas dire qu'il
faut négliger la dernière partie « autres ».

### Dormir

On en vient au sac de couchage, j'ai pris un 15° et le risque qui va avec de se
geler un peu par moments. Mais en fait ça va, en plein juillet. Compact et fait
le taff qu'on lui demande.

Dormir à même le sol, c'est un peu con, ça fait mal au dos. Donc il faut un 
matelas. Un matelas en mousse type gym, c'est nul, ça prend beaucoup de place
et en extérieur ça prend la flotte. Y'a des matelas gonflables assez compacts 
chez decath ou similaire, léger et resistant. En plus on dort pas trop mal 
dedans !

Autour de tout ça, on a la tente. Une tente 2 secondes, c'est pas pour nous.
On veut un truc compact et léger. Autant une ultra light c'est pas mal mais
faut avoir envie de lâcher 100+€. Non, la tente de merde à 30€ chez decath 
c'est suffisant. Après un peu d'entraînement il suffit de 5 minutes pour la 
monter ou démonter. En plus on apprend vite que tout attacher c'est surfait.
Par contre ça rentre pas dans le sac, faut l'attacher à l'extérieur. C'est 
parfait si on veut donner des coups de tente aux gens dans la rue sans faire 
exprès !

On sait jamais où on va dormir ni quand, donc boules quies et de quoi se bander
les yeux. Y'a rien de pire que de se faire réveiller à 6h du mat parce qu'il 
fait jour ou parce qu'un bus passe à côté de la tente…

### Fringues

Ensuite des fringues. Classique : plusieurs t-shirts, calcifs, chaussettes.
Environ 3 de chaque ça suffit, suffit de laver quand on peut. Un seul pull ça
suffit, pareil pour la veste. Mais une veste contre la pluie. On va pas faire 
le difficile, on part pas pour être fashion. Ça empêche pas d'emmener une 
chemise casual, ça fait du bien à porter quand il fait chaud. Pareil pour les 
débardeurs. Pour les pantalons, prévoyez un jean et genre un short ou deux, ça
prend pas trop de place. Vous serez contents d'être respectivement au chaud et
au frais quand y'en aura besoin.

Niveau chaussures j'avais que des chaussures de rando. C'est confortable, 
étanche et c'est du coup fait pour marcher. Avec du recul ça aurait été bien
genre des tongs ou sandales.

### Nourriture

Avec ça, deux bouteilles d'eau : une de 1,5L ou plus et une de 50cl. La petite
sert à remplir la grande via les robinets relous des restos ou aires. On y
pense pas avant d'avoir l'air con. De toute façon ce type de voyage t'as l'air
con tout le temps.

Niveau bouffe osef un peu, on en trouve partout. L'eau c'est disponible 
gratuitement donc ça serait con d'en racheter. Le manger c'est plus compliqué.
Rien n'empêche d'emporter des gâteaux secs pour le soir ou les marches hors 
ville.

### Outils

Ça c'est encore une fois pour éviter d'avoir l'air con : des cartes. Une carte
d'Europe suffit amplement au début. C'est pour quand un mec te dit « Je vais à
Helmstedt, tu veux venir ? » tu puisses répondre « Ah oui oui c'est sur mon
chemin ».

Après les cartes plus précises ça se chope en office de tourisme. Ou on peut 
demander aux gens dans la rue, ça marche aussi.

Un couteau suisse, super utile ! Que ça soit pour rafistoler votre sac, votre 
pantalon — RIP short — ou faire un sandwich, ça sert obligatoirement.
Par contre un marteau, ça pas forcément. Pour la tente j'ai cru que j'en aurait
besoin mais au final les sardines s'enfoncent souvent très bien à la main. Au
pire une caillasse ça se trouve !

On oublie pas d'ajouter une lampe frontale avec quelques piles. Quand on 
cherche un coin dodo c'est relou quand il fait nuit. Pareil pour planter la 
tente et chill à l'intérieur. Et puis ça sert pour tout type d'aventure une 
lampe.

Pour faire du stop, faut faire des signes. C'est toujours utile de choper du 
carton dès qu'on peut. Vous voyez du carton sympa en marchant ? Prennez le ! 
Avoir dans les 3 signes d'avance c'est toujours cool. Si on vous dépose dans un
coin paumé sans rien, faut pouvoir réagir de suite.
Pour aller avec ça, il faut un marqueur. Le genre de marqueur indélébile assez
gros. On veut pas passer 15 minutes à faire un panneau.

### Passer le temps

Quand on voyage comme ça, on a pas vraiment du courant à disposition. Donc exit
l'ordinateur, emportez votre 3310 préféré, de quoi faire des photos et c'est
tout. Avec ça on s'amuse un peu, le temps de retrouver un macdo pour recharger 
les appareils. C'est un peu pour ça que j'ai pas beaucoup de photos.

Du coup, on emmène des livres ! Ça sert tout le temps : en journée comme au 
moment d'aller dormir. Perso j'ai bien aimé noter tout ce que je faisais dans
un carnet. C'est pas forcément exhaustif, mais ça permet de me rappeler de 
certains trucs. Et du coup d'écrire ce même petit journal.

Après faites vous votre kiff. Si vous adorez faire du yoyo quand vous vous
emmerdez, go.

### Autres

Tiens si on part en été, on arrive sur la crème solaire. Faut surtout pas
l'oublier celle là, sans quoi on a bien l'air con. Autres produits d'hygiène
classique : brosse à dent, dentifrice, déodorant. Pensez aussi aux mouchoirs,
on se dégueulasse ou coupe assez facilement.

Niveau portefeuille, ne prennez que le nécessaire. Genre la carte fidélité du 
kebab d'à côté on s'en balance. Le minimum c'est plutôt carte bancaire, carte 
d'identité ou passeport et carte européenne d'assurance maladie (CEAM).

Lunettes de soleil aussi ça peut être intéressant, avec une casquette.

Et très important, j'ai failli oublier : une montre ! Exit les téléphones, ça a
plus de batterie assez vite.

\newpage

## Où et comment dormir

Cette partie va encore une fois être très subjective. Où et comment dormir, ça
dépend beaucoup de l'état de fatigue, du terrain, de la ville et de plein de
trucs.

Donc en gros où dormir : dans un endroit à l'écart déjà. C'est à dire pas dans 
un parc public ! En périphérie des villes on trouve facilement des champs ou
des allées pas très fréquentées en banlieue avec de l'herbe. Exemple : une fois 
j'ai planté la tente entre un panneau publicitaire et un petit bosquet, à côté 
des lignes de tram. C'était du coup plutôt abrité et calme malgré 
l'emplacement, en pleine ville.
Il suffit de rester respectueux des gens autour et tout devrait bien se passer.

Après le top ça reste les aires d'autoroute ! Je partais souvent d'une ville en
fin d'après-midi, ce qui me permettait d'avoir une voiture assez facilement. À
partir de là bouffe et toilette sur une aire d'autoroute. Plantage de tente un
peu en retrait, souvent près du parking à camions. Y'a moyen d'être vraiment
tranquille et tout est à disposition.

Pour les infos concernant la tente, lire un peu plus haut la partie matos ↑.

\newpage

## Comment se nourrir

Plutôt facile de se côté là si vous décidez de bien vous organiser.

L'eau, c'est le plus compliqué. Dès que vous vous arrêtez dans un resto, une 
aire d'autoroute ou n'importe quoi, PRENNEZ DE L'EAU. Même si c'est juste pour
remplir 20cl dans une petite bouteille, c'est important, ça servira.
Y'en a même dans la rue, y'a des fontaines d'eau potable !

Côté manger, faites vous plaisir. Des gâteaux qui craignent pas trop la chaleur
dans le sac pour un petit goûter, voire petit dej. Pour les repas soit on mange
vite fait ce qu'on trouve dans un supermarché, soit on attend d'être dans les 
pays de l'Est pour se payer un resto à 6€ !

Je crois que mon budget le plus important a été la bière. Je m'arrêtais presque
tous les jours pour une petite pinte. C'est pas de l'alcoolisme, c'est de la 
découverte de traditions et cultures (DTC).

\newpage

## Hygiène

Ça c'est vraiment la question qui revient le plus : comment faire pour rester
propre ? C'est vrai que durant ce type de voyage on a pas vraiment d'endroit où
dormir et du coup où se doucher.

Mais c'est pas grave ! À l'arrache on trouve toujours un moyen. Ça peut aller
du rinçage de tête dans la fontaine, à la baignade dans une rivière. 

Pas mal d'aires d'autoroute peuvent aussi vous filer une clef pour prendre une 
douche gratos si vous demandez gentiement. Le monde n'est pas rempli de 
connards qui sont attachés aux règles et veulent faire du profit. Vous 
inquiétez pas, les gens sympas ça existe toujours.

Ensuite comme déjà dit précédemment, les fringues ça se lave ou au pire une 
couche de déo si vous avez pas peur.

Après qu'on soit d'accord, on peut pas prendre une douche tous les jours. C'est
pas vraiment possible à moins de passer son temps à en chercher.
Donc on apprend à vivre dans la crasse, c'est bien on dirait un vrai hippie
après. Et puis votre nez va s'habituer à l'odeur. Vous, ça vous gêne pas, et 
les autres auront l'impression d'avoir affaire à un vrai aventurier. Tout
bénef.

\newpage

# Choisir sa voiture et où se placer

Savoir où faire du stop, c'est plutôt intuitif mais on sait jamais. Y'a
toujours des guignols qui font du stop assis en fumant une clope…

## Attitude

L'attitude, oui, c'est primordial ! Vous êtes une vitrine, vous voulez que les 
gens s'arrêtent. Si vous faites douter ne serait-ce qu'un peu l'automobiliste,
il va passer son chemin, logique.

Donc on range la casquette, les lunettes pareil, même si t'a grand soleil.
Souriez quand les gens passent, ça instaure confiance, soyez pas crispé.

J'ai aussi remarqué que ça marche mieux avec le sac sur le dos, tout du moins
quand il est bien en visu. Le laissez pas traîner derrière vous, non, montrez
bien que vous avez un gros sac et que vous voyagez vraiment. Ceux qui 
pourraient avoir peur seront rassurés.

## Sur la route

Savoir se placer quand on fait du stop sur la route, c'est plutôt important. Si
vous vous placez dans un virage, personne vous verra, personne s'arrêtera. Par
contre sur une grande avenue avec des places un peu en retrait pour des bus par
exemple, ça c'est bon !

Le but ici est d'être vu assez tôt et qu'il y ait assez de place pour que les
voitures s'arrêtent. C'est pas toujours évident, auquel cas on se rabat sur les
feux rouges. Là avoir il y a deux méthodes : soit vous montrez votre panneau en
espérant qu'on vous voit, soit vous allez à la recherche de la voiture la plus
intéressante pour vous.

Du coup on fait un panneau GRAND et LISIBLE. Oubliez tout ce que vous avez vu
sur instagram. Pas de dessin, c'est débile. Écrivez juste la ville, l'autoroute
voire le pays où vous voulez aller. Mais soyez un minimum précis. Le gars qui 
écrit "ANYWHERE" sur son panneau, je suis pas dit que beaucoup vont s'arrêter…

## Sur une aire ou station service

Quand le trafic est à l'arrêt, c'est encore le plus simple d'après moi. Vous 
avez l'occasion de parler à tout le monde, profitez en !

Rien que quelques mots dans le style :\
```
    Excusez moi, bonjour. Je cherche à aller en Belgique, est-ce que vous 
    pourriez m'emmener à la prochaine station-service ?
```

Le fait de demander peu mettre les gens en confiance, vous aurez généralement
pas trop de souci. Si vous me demandez par exemple d'aller directement Gent par
contre, je saurais pas forcément dire si le bled où je vais est sur le chemin.

Si jamais l'aire est trop grande, du style avec des restaurants, une pompe à
essence etc., il peut être plus avantageux de se placer à la sortie. Même règle
que pour la route : un endroit dégage et visible, panneau lisible.

## Choix de la voiture

Choisir la bonne voiture, c'est choisir la voiture qui va dans notre direction.
C'est plutôt facile en France, on peut voir les départements sur les plaques.

Si vous voyez une voiture du département où vous êtes, c'est sûrement un local,
concentrez vous sur les autres. On rate peut-être quelques voitures comme ça,
mais on en trouve une sûrement plus vite.

Ce qui se corse, c'est dans les autres pays… Hésitez pas à demander aux offices
de tourisme ou simplement aux gens dans la rue.\
En Allemagne et Pologne par exemple, on retrouve (à peu près) les initiales de
la ville de la voiture. En République Tchèque une lettre qui indexe la ville,
du style "4AUT 2700", le A veut dire Prague.

Une fois qu'on a compris ça, ça devient plus facile !

\newpage

## Autres astuces

Le temps d'attente est en général assez faible. Autant on peut attendre 
longtemps par moments, autant c'est souvent rapide ! Vers la fin de ce papelard
j'ai mis mes stats.

Pour les autoroutes en Allemagne, y'a une règle simple : les nombres pairs vont
d'Ouest en Est, les nombres impairs du Sud au Nord. C'est plutôt pratique pour
se repérer quand on vous dit "Je prends l'A14" → boum nord-sud.

## Une fois arrivé

Quand la voiture vous dépose, il y a quelques trucs à faire impérativement. Je
parle pas de politesse avec le chauffeur, ça c'est a priori normal quand on est
pas débile.

Non, là je parle de l'office de tourisme. Si vous vous trouvez sur une aire
d'autoroute, venez relire ce passage une fois arrivé en ville. Enfin bon, y'a
trois trucs essentiels à chercher :

  - plan de la ville
  - autoroute la plus proche, parfois pas sur le plan car trop loin
  - toilettes, faut remplir les bouteilles et pisser oui

Faut pas oublier qu'on est là pour visiter aussi ! Toutes les brochures sont
bonnes à prendre tant que c'est gratuit. Pareil pour les plans ! Dans certaines
villes la carte du centre ville coûte 2€, dans d'autres le pays complet est 
gratuit…

\newpage

# Mon voyage

## Re-introduction

Je m'ennuyais beaucoup durant ces vacances, j'avais en tête de partir pour la 
Pologne, sans vraiment savoir par où passer.
Après avoir discuté avec quelques amis, je me rends compte qu'ils sont à Bruges
et iront à Bruxelles le lendemain.
C'est tout bon, je sais déjà vers où partir, direction la Belgique !

\newpage

## Belgique

\backgroundsetup{
scale=1,
color=black,
opacity=0.9,
angle=0,
contents={%
  \includegraphics[width=\paperwidth,height=\paperheight]{images/belgique_header.jpg}
  }%
}

\thispagestyle{empty}
\BgThispage
\clearpage

### 22/06/16

J'ai mis beaucoup de temps à me décider. J'avais déjà préparé mon sac la 
veille, je manquais juste de courage. C'est un peu compliqué de se dire qu'on
va partir tout seul avec sa tente et finir à l'autre bout de l'Europe une
semaine plus tard.

* 17:00\
  Je me décide enfin, je prends le métro à Villejuif puis le tram.
* 18:00\
  Arrivée à Porte de la Chappelle, là où les voitures ont le choix de prendre
  le périphérique, d'aller à St-Denis ou d'aller vers la Belgique. C'est un
  endroit génial pour le stop, les voitures sont amassées sur plusieurs voies
  derrière un feu rouge. On peut facilement voir qui vient du nord et peut-être
  même aller leur parler. J'ai récupéré du carton près d'une poubelle, j'avais
  mon marqueur, tout au bon endroit j'vous dit ! Ici, j'ai décidé de marquer
  **Lille**.
* 18:10\
  Même pas 10 minutes à tenir mon panneau, un mec au feu m'appelle pour me dire
  qu'il va à Lille. En fait, il allait à Dunkerque voir des potes à lui, il
  a donc pu me déposer sur une aire avant Lille. On a eu aucun problème
  à trouver des sujets de discussions, il était par exemple motard tout comme
  moi.
* 19:40\
  Arrivée à l'aire de Wancourt-Est, avant la bifurcation pour aller
  à Dunkerque, environ 50km de Lille. Seul bémol, j'ai pas fait gaffe, il m'a
  déposé après la bifurcation qui va à Bruxelles. À partir de là, impossible
  d'aller à Bruxelles directement, il faut passer par Gent.
  On recommence donc à faire du stop sur l'aire ! Pas de panneau ici, je me 
  suis juste mis devant l'entrée du magasin de la station service.
* 21:45\
  Il aura fallu un peu de temps pour obtenir une voiture, mais c'est chose 
  faite. Le mec peut me déposer à **Gent**, c'est parfait. En plus mes potes
  passent par là le lendemain pour aller à Bruxelles !
* 23:00\
  Je suis maintenant à 5km de Gent, en banlieue. Il est tard, j'ai envie de 
  poser la tente. Je me balade un tout petit peu et je trouve un coin de forêt
  à côté de la route. Un peu bruyant mais comme il pleut je me dépêche.
* 23:30\
  Au dodo ! À peine ma seconde nuit dans cette tente mais ma première du 
  voyage, qui plus est dans une ville à l'arrache !

\newpage

### 23/06/16

* 09:40\
  Réveil. Il pleuvait encore. En sortant de ma tente : des ouvriers. Je m'étais
  posé à côté d'un transformateur électrique… Ça avait pas l'air de les gêner,
  ça reste étrange. 
  Je range mes affaires et me dirige ainsi vers le tram pour aller en ville.
  J'étais pas loin, j'aurais pu faire ça à pied au final, m'enfin !\
  Je vous épargne tous les détails, en gros je me balade, je prends des photos,
  je mange un bout et je vais me poser là pour lire un livre :

  ![Vrijdagmarkt](images/gent.jpg)

  Gent, c'est vraiment une ville magnifique, il y a énormément de bâtiments 
  super classes à voir ! Pour éviter de faire un bouquin de 100 pages, juste
  quelques photos du coup.

  ![J'ai oublié ce que c'était](images/belgique_eglise.jpg)

* 17:30\
  Je m'arrête dans un resto pour boire un coup. Les prix sont corrects, le
  choix des bières également. On est en Belgique après tout.\
  Au programme :
    * \textit{Gentse Gruut Blond} : bière blonde locale. De mémoire le houblon
    est ici remplacé par d'autres herbes, rendant cette bière légère avec un
    goût sympa.\
    Note de 4/5 sur l'échelle internationale de mon classement.
    * Petrus Speciale : c'est là qu'on voit l'importance de garder des notes
    pour se rappeler de toute la saveur de la bière. J'ai marqué : « sans grand
    plus mais pas mauvaise »…\
    Un 4/5 quand même ! Je saurais plus dire pourquoi par contre !

    ![Bière locale, la Gruut](images/gent_biere.jpg)

* 20:00\
  Arrivée de mes potos ! On va bien entendu boire une bière, discuter un peu.
  Puis ces lâcheurs se barrent peu après, ils avaient une heure de 
  correspondance. Ça fait plaisir de croiser des têtes familières un peu à 
  l'improviste dans un autre pays !

\newpage

## Pays-Bas

\backgroundsetup{
scale=1,
color=black,
opacity=0.9,
angle=0,
contents={%
  \includegraphics[width=\paperwidth,height=\paperheight]{images/paysbas_header.jpg}
  }%
}

\thispagestyle{empty}
\BgThispage
\clearpage


### 23/06/16 — Suite

  Toujours le même jour, il est temps de bouger !

* 20:40\
  Une fois partis, moi j'me mets en route pour me barrer de cette ville. 
  Demain, ils seront à Amsterdam, il faut que j'y sois aussi.
  Je trouve assez vite une bretelle — après 20 minutes de marche — qui va sur 
  l'autoroute et j'me pose là, il pleut qu'un peu, ça va.

* 21:45\
  J'allais me démotiver et commencer à sortir un sandwich pour manger, quand
  une voiture s'arrête, se garre et le conducteur me fait signe.
  C'était en fait une hollandaise qui voyage pas mal en Belgique pour voir des
  potes. Elle me fait un peu de place et on part pour Breda, une ville juste
  après la frontière.\
  Elle est extrêmement sympatique, elle me file même un CD d'une de ses amies
  chanteuse ! J'ai rien compris, c'est en néerlandais…

* 22:45\
  On arrivé donc à Breda, elle me laisse sur une aire d'autoroute. À partir de 
  là, c'est classique : manger, boire, toilette.
  Je recherche un petit coin dodo et après avoir erré je me pose dans un champ
  à côté de l'aire. Étant pas doué, j'ai posé la tente sous une éolienne, en
  bord de route, à côté d'un dépot de camions. Merci les boules quies… En plus
  j'ai failli me faire attaquer par des oies !

\newpage
 
* 23:50\

  ![Philosophie](images/next_thing.jpg)

  Fin de la journée !\
  J'ai trouvé un mur sympa en me baladant. De mémoire c'est à côté d'un musée
  assez contemporain, ça fait sens. La phrase en elle même aussi. Si j'étais
  un gros con sur facebook j'aurais dit "à méditer".


\newpage

### 24/06/16 

* 08:10\
  Premier réveil de ma vie dans les Pays-Bas, grand moment. Non en fait j'ai
  mal dormi et j'range ma tente.

* 09:20\
  On va donc essayer de partir pour Amsterdam, c'est parti on va demander à
  tout le monde.

* 10:25\
  Un mec va à Rotterdam, c'est sur la route, c'est pas plus mal ! Il va 
  participer à un tournoi de tennis là-bas. D'ailleurs il m'apprend que 
  « Gent », ça se prononce "Rent" avec le R roulé. Je comprends mieux pourquoi
  personne me comprennait en Belgique…

* 10:55\
  Hop, Rotterdam bonjour. Ou du moins sa banlieue, j'ai 1h de marche pour 
  arriver au centre. Ce qui se fait plutôt facilement, même s'il pleut 
  \textit{encore} !\
  La ville est très design, c'est pas trop mon genre. Faut l'avoir vu quand 
  même une fois dans sa vie je suppose…

  ![Bâtiment chelou](images/rotterdam.jpg)

* 14:15\
  En essayant de partir, je trouve un biergarten dans une rue très moche. 
  J'aurais dit que c'était un vieil entrepôt mais non, y'a de la bière ! Zone
  assez atypique, le service se fait dans un container et tout est très coloré
  dans une cour intérieure\footnote{http://biergartenrotterdam.nl/}.

  ![Les chiottes, dans le container à côté](images/rotterdam_biere2.jpg)

  Au programme :

  * Gulpener Ur-Weisse, une bière blanche. Aucune trace de notes…

  ![Bière blanche locale](images/rotterdam_biere.jpg)

* 14:40\
  En route pour le stop ! On m'a indiqué une station service juste avant le
  début de l'autoroute, c'est par là que je vais.

* 15:25\
  Arrivée donc à cette station, je pense déjà à poser mon sac et fumer une 
  clope. Même pas le temps ! Un gars me regarde et me demande si je vais à 
  Amsterdam, bien entendu je réponds oui, ce à quoi il me dit « ok, je finis de
  payer et on y va ». C'était là très facile ! Le mec est électricien et bosse
  à Rotterdam et Amsterdam, il fait presque tous les jours le trajet.

* 16:30\
  On arrive à perpette d'Amsterdam, le mec me dépose avant d'aller à son taff.
  Il y a apparemment une bonne marche jusqu'à la gare, pour ensuite aller au
  centre…

* 17:05\
  En effet, après un petit bout de marche et un train on arrive à la gare
  centrale. C'est galère les routes là-bas, faut faire attention, y'a des
  scooters sans casques sur les pistes cyclables. Et c'est totalement légal.
  Déjà qu'il y a 50 000 vélos et que tu manques de mourir écrasé absolument
  tout le temps…\
  C'est là que j'y retrouve mes amis de la veille ! On se balade tranquillou,
  on va boire une bière et c'est le moment de leur dire au revoir, ils 
  repartent je sais plus où.\
  Amsterdam, c'est une très belle ville ! Des canaux un peu (surtout) partout,
  de beaux bâtiments. Par contre c'est rempli de monde en été.
 
  ![Plein de monde !](images/amsterdam2.jpg)

  Bien entendu, on est à Amsterdam, tout le monde y pense, on s'arrête dans un
  coffee shop ! C'est une putain d'attraction touristiques là-bas… Ils vendent
  des joints déjà roulés et de quoi rouler sans se rater. On sent aux questions
  des vendeurs que leur clientèle c'est surtout des gens qui veulent tester.
  M'enfin, j'ai pris peu, mais ça m'a quand même tenu presque jusqu'à la fin
  du voyage…

* 22:10\
  Je vais me poser au bord d'un canal pour écouter un peu de musique et lire.
  C'est sympa la nuit là-bas, beaucoup d'ambiance. Par contre faut faire gaffe
  au quartier rouge, y'a des choses pas très plaisantes pour les yeux par 
  moments…

  ![Petit canal à Amsterdam](images/amsterdam.jpg)

  Pour le moment, je ne sais toujours pas où je vais dormir. Je vais arrêter de
  le répéter je pense, je sais jamais au final où j'vais finir !
  Cette fois il est quand même assez tard, j'vais pas tarder à me mettre en 
  route.

* 00:45\
  Après avoir parcouru plus de 2km à partir du centre, je suis toujours dans 
  la ville ! Bordel c'est grand ici.
  Je suis dans un quartier résidentiel, avec un parc plutôt sympa. J'ai trop la
  flemme d'aller plus loin, je pose mon sac sous un banc. Ensuite c'est mon sac
  de couchage et moi-même que je pose sur ledit banc, et c'est dodo.

\newpage

## Allemagne

\backgroundsetup{
scale=1,
color=black,
opacity=0.9,
angle=0,
contents={%
  \includegraphics[width=\paperwidth,height=\paperheight]{images/allemagne_header.jpg}
  }%
}

\thispagestyle{empty}
\BgThispage
\clearpage

### 25/06/16

* 09:50\
  Comme tous les jours : réveil. Je me rends compte que c'est beaucoup plus
  facile sans tente, moins de bordel à ranger ! Bon par contre j'me suis fait
  réveiller par un chien qui m'a aboyé dessus. Sa maîtresse m'a regardé en 
  souriant, je sais pas si c'était parce que j'avais l'air d'un clodo ou juste
  d'un mec paumé fatigué. Je penche pour le second choix, ça m'aurait aussi
  fait sourire.\
  M'enfin, j'me dirige vers l'autoroute à coup d'abri bus pour avoir une carte.

* 11:30\
  Ayé, je brandis fièrement mon panneau avec l'autoroute de marqué dessus. Ce
  qui est chiant à Amsterdam, c'est que c'est grand. Ils ont aussi un périph
  là-bas : les Ring. Du coup une bagnole sur je sais pas combien prend 
  l'autoroute, surtout quand on veut aller en Allemagne…

* 12:20\
  Un peu long, mais une voiture me prend, on est parti pour environ 60km. C'est
  franchement pas mal, la frontière doit être à environ 160km à peine.

* 12:55\
  À partir de là, pas très intéressant, j'arrive vers Apeldoorn.
  Manque de bol, le mec me dit que c'est une très grande aire. Alors 
  effectivement, c'est une grande aire, mais pas du bon côté ! De notre côté,
  c'est juste un parking avec une galerie sous l'autoroute pour traverser à 
  pied. Du coup personne s'arrête pour prendre l'essence, heureusement c'est
  l'heure de manger…
  À partir de là, plus question de chercher quelqu'un pour m'emmener à la
  frontière, on compte quatre voitures sur le parking à tout casser. 
  Le but est de quitter cette aire à la con. Après une quarantaine de minutes 
  je trouve un gars qui peut m'emmener deux aires plus loin. Très sympa, il
  conduit une vieille Opel Commodore en très bon état pour son âge ! Ce gars
  m'a même donné une petite bouteille d'eau qui s'est révélée très pratique.

* 14:00\
  Aire d'autoroute beaucoup plus confortable ! Pas grand chose à dire, je 
  cherche une tuture…

* 14:50\
  Départ vers Hengelo, une ville juste à côté de la frontière ! On y arrive
  une trentaine de minutes plus tard. Il est encore bien tôt, on va pouvoir 
  arriver en Allemagne sans aucun problème.

* 17:30\
  Alors là… c'était bien long ! Personne ne va en Allemagne faut croire. Par 
  contre j'ai pu pour la première fois tester le camion ! Peu de camioneurs 
  sont en général d'accord pour prendre du monde, question d'assurance, mais 
  ici il avait l'air d'en avoir rien à faire. De mémoire, le gars allait à 
  Dresde, ce que je voulais aussi faire mais je voulais m'arrêter à Hanovre 
  avant.

* 19:50\
  On arrive à 40km d'Hanovre. Le chauffeur doit dormir, c'est les règles. Vu 
  que la station est un peu déserte à cette heure, je vais faire pareil.
  Deux heures après, le temps de manger un bout et tout, je pose la tente et
  c'est dodo. Demain, c'est Hanovre !

\newpage

### 26/06/16

* 10:45\
  Pas de notes avant ça. Après quelques jours on a la flemme. Là, y'a juste
  marqué « rangement du camp après avoir lu un peu ».

* 11:25\
  Le temps de se réveiller et tout, je commence à aller demander à un peu tout
  le monde s'ils vont à Hanovre. À peine dix minutes plus tard je croise un
  couple d'allemand dans la soixantaine qui semblent habiter à Berlin. Ils vont
  effectivement à Berlin et veulent bien me déposer un peu plus près.

* 12:00\
  Eh bah en fait, ils m'ont déposé au centre d'Hanovre ! À côté d'un métro, ou
  bien à 15 min à pied du centre, au top. Le truc c'est qu'ils allaient à 
  Berlin, c'est des fous. Et puis à 180km/h dans une gros Mercedes, ça va vite.
  \
  Encore une fois, très belle ville ! J'vous laisse contempler les photos, moi
  j'adore ce style.

  ![Petit concert](images/hanovre.jpg)

  Peu après être arrivé au centre, un concert de musique classique en plein air
  près de la gare. Aucune idée de ce que c'était, ça ressemblait à du 
  romantique.

  ![Église qui claque](images/hanovre2.jpg)

  Vu qu'il est midi passé, il se fait faim. Mais surtout soif. Je rappelle
  qu'on est en plein été et qu'il faut s'hydrater.

\newpage

* 14:15\

  ![Bière peut-être du coin](images/hanovre_biere.jpg)

  Voilà donc que je m'arrête boire un coup, faut bien. Ça, c'était une König
  Ludwig Weizen Hell. Une blanche sans trop de prétention : 3/5.
  J'en profite pour lire, le cadre est sympa. J'ai vu sur l'église présentée
  juste avant.\
  Par contre, c'est l'Euro 2016 et y'a un match de foot. Les serveurs sont des
  connards et me demandent de bouger. Donc je bouge et j'vais me poser au bar
  d'à côté avec une Franziskaner parce que c'était pas cher et que j'avais 
  envie de finir quelques chapitres de mon livre !

\newpage

  ![Lac sympathique](images/hanovre3.jpg)

  Après ça, je pars me balader, y'a plein de trucs à visiter.
  Comme ce lac, c'est magnifique les environs.\
  D'ailleurs en chemin, je trouve ce petit bar. Si j'avais su avant…

  ![Quelques bières artisanales](images/hanovre_biere2.jpg)

  ![Feu tricolore codé par un stagiaire](images/hanovre_velo.jpg)

\newpage

  C'est pas tout mais j'vais déjà partir pour Dresden. Séjour de courte durée
  ici, j'avais pas l'impression de pouvoir faire grand chose. Ville très jolie
  par endroits mais j'me suis vite ennuyé. J'ai sûrement pas assez cherché.

* 19:25\
  Début donc du stop pour Dresden.

* 19:35\
  Une voiture me prend, on part vers Magdeburg. Magdeburg, c'est en gros la 
  ville à la bifurcation de l'autoroute qui va à Berlin ou Dresden. Du coup 
  c'est là qu'on a envie de s'arrêter si le gars va à Berlin, logique.

* 20:12\
  On arrive à une cinquantaine de kilomètres de Hanovre, c'est pas ouf mais
  au moins je suis bien avancé sur l'autoroute !

* 20:25\
  Bon bah du coup on est à Braunschweig, une aire en dehors de l'autoroute avec
  surtout des fastfood et un cinéma. Le type de cinéma de zone de loisirs pour 
  plusieurs petits villages de campagne. Vous savez, le type de cinéma où y'a 
  des rassemblements sur le parking, au top.

* 21:05\
  Après une bonne demi heure d'attente, c'est tipar pour avancer un peu. Encore
  une fois, on avance de 50km. Faut pas perdre de vue que 50km, c'est toujours
  une victoire !
  De mémoire, une meuf avec sa fille et sa mère dans une petite polo. Très 
  gentilles même si je comprennais pas tout…

* 21:25\
  Bon bah hein voilà ça a pas duré longtemps le trajet. Petite aire 
  d'autoroute, juste de quoi recharger mes téléphones, lire un peu et manger 
  un sandwich trop cher.
  Ici, l'aire était trop petite pour balancer la tente. Je suis allé dans le
  village, c'était collé. J'ai posé la tente dans une allée avec de l'herbe et
  assez tranquille. Les gens avaient l'air amusés en me voyant le matin.

\newpage

### 27/06/16

* 09:45\
  Petit réveil comme d'habitude, rituel de démontage de tente et redirection 
  l'aire. Ici, j'ai eu de la chance : je trouve 5€ par terre en attendant 
  derrière mon cailloux favori, b00m.
  L'avenir appartient en effet à ceux qui se lèvent tôt, en l'occurence avant 
  10h.

* 10:43\
  Après même pas 10 minutes de stop, une voiture me prend, ça fait plaisir. 
  Gros classique en Allemagne : le mec est allemand. Eh ouais, et comme il est 
  allemand, on fait péter les stéréotypes et on peut supposer qu'il avait une 
  VW Passat et qu'il roulait à 210km/h tout le long sur l'autoroute. C'était 
  assez le cas en plus.
  Bon on va pas loin hein, encore une 40-aine de kilomètres, juste histoire
  d'arriver avant la bifurcation Berlin-Dresden.

* 10:46\
  w00h 15 minutes de trajet, ouf. Par contre j'arrive dans une grande aire, 
  j'ai plutôt confiance. Et quand t'as trop confiance, la vie Mino elle te 
  prend, elle te nique\footnote{Fatal Bazooka - Chienne de vie}.\
  Alors en effet, gros bourbier, j'attends des heures sans succès. Certaines 
  personnes s'arrêtent pour me filer de la crème solaire ou de l'eau, big up 
  les potos. Mais personne pour faire un bout de route !\
  À un moment un autostopeur arrive, un allemand. Sympa mais un peu frimeur. On
  se sépare pour couvrir plus de terrain sur l'aire. Et vous savez quoi ? Ce
  guignol chope une voiture avec une seule place de dispo et se barre avec !\
  Allez, rebelote l'attente. Au final ce sont deux jeunes qui reviennent d'un 
  festival vers Hamburg qui s'arrêtent, et ce au bout de 4h30 d'attente.

* 15:20\
  C'est l'heure du fucking départ pour Dresden bitch. En plus on va vraiment à 
  Dresden centre ! On a écouté un peu tout et n'importe quoi dans la voiture, 
  ils étaient assez ouverts. Après un festoche sous la flotte ils étaient 
  contents d'être dans une voiture pépère. Et puis 230km environ, c'est 
  toujours mieux avec des gens cools.

* 18:15\
  Hop hop hop, plein centre de Dresden, ça me rappelle plein de souvenirs, 
  j'y étais passé l'été dernier aussi. On tombe en pleine manif antifa, 
  l'occasion rêvée pour taxer en toute politesse quelques bières aux 
  manifestants.
  En vrai c'était plutôt une contre-manif contre les immigrés, assez louable 
  mais peu de monde.

  ![Manif en plein centre](images/dresden.jpg)

* 19:15\
  Bon du coup petit résumé de la journée : il a fait chaud, 4h d'attente au 
  soleil, mais on est à Dresden. Du coup on fait quoi ? BAH ON BOIT.
  Petite binche en terrasse, au même endroit que l'an passé. Par contre cette 
  fois je rends les verres, j'en ai trop.

  ![Petite pils oklm](images/dresden_biere.jpg)

* 00:25\
  Je connais déjà un peu Dresden, je repars donc pour l'autoroute histoire de 
  poser la tente. Bonne nuit.

\newpage

## République Tchèque

\backgroundsetup{
scale=1,
color=black,
opacity=0.9,
angle=0,
contents={%
  \includegraphics[width=\paperwidth,height=\paperheight]{images/tchequie_header.jpg}
  }%
}

\thispagestyle{empty}
\BgThispage
\clearpage

### 28/06/16

* 10:30\
  Une heure après le réveil, c'est l'heure d'essayer de partir pour Prague.

* 13:00\
  Après environ 2h de stop, parce que faut bien faire des pauses tu sais, une
  voiture s'arrête. La meuf a des chevaux en République Tchèque et fait souvent
  l'aller-retour. Du coup elle peut me déposer la frontière. C'est à genre 40km
  mais encore une fois ça avance pas mal.

* 13:20\
  Qu'est-ce que je disais, on est déjà arrivés ! Mais ici c'est ZE BEST aire
  de la vie. La vignette d'autoroute est obligatoire, donc tous les péquenots 
  qui l'ont pas, ils s'arrêtent ici pour l'acheter. \
  Il suffit d'attendre que les gens ressortent de la petite cabine et on peut 
  commencer à les aborder. 

* 13:45\
  Quelques refus classiques, mais très vite un couple est assez chaud. Ils vont
  passer un petit moment en amoureux à Prague, parfait.\
  On prend plein de routes de merde, forcément le GPS a décide de s'amuser.

* 15:00\
  On arrive quand même à Prague, et en plein centre encore une fois. Ils ont
  leur hôtel assez bien situé.\
  À partir de là, quand on est parachuté en plein centre ville, faut se 
  repérer. Du coup je demande aux gens et je tombe sur un québécois qui crame
  tout de suite mon si bel accent anglais. Bon du coup on en parle en français 
  ça marche aussi et j'me démerde pour trouver un office de tourisme.

  ![Place centrale](images/prague.jpg)

  Là, c'est à peu près le centre, de jolis bâtiments et des québécois.

* 17:40\
  Il fait chaud, super trop chaud. En plus à Prague y'a moyen de monter sur la 
  colline ainsi que sur les églises, ça aide pas.

  ![Vue d'une tour qu'on grimpe avec un sac de 15kg](images/prague_tour.jpg)

  Après être redescendu, il fait soif, donc arrêt obligatoire au bar. Dans un 
  coin un peu plus tranquille, j'en profite pour lire comme d'hab.

  ![Allez, la Pilsner Urquell](images/prague_biere.jpg)

  Je crois qu'on m'a viré parce que j'étais resté trop longtemps ici…

* 20:10\
  Quand il commence à pleuvoir et qu'on a nulle part où dormir on fait quoi ?
  Bah on va au MacDo. Ici on recharge son téléphone et on regarde les gens 
  galérer dehors. Par contre on y mange pas hein, y'a des restos moins chers !

* 22:10\
  Euh ouais j'ai passé 2 putains d'heures au domac à lire et charger 
  péniblement mes téléphones. Mais c'est pas grave, direction resto !\
  Après une petite vingtaine de minutes, j'arrive dans une petite taverne qui 
  sert encore. Très convivial, on peut même fumer dans le resto wtf la Tchéquie.
  Du coup le repas + la bière, environ 10€, sympa.

* 00:30\
  Ça, c'est l'heure du dodo. Après avoir parcouru Prague un peu j'me rends 
  compte que c'est trop galère de sortir. Au mieux j'me retrouve à dormir sur 
  un banc comme à Amsterdam. Donc j'me pose dans un vieux buisson qui pue la 
  merde près d'un pont. Un bon squat…

\newpage

### 29/06/16 

* 09:15\
  Allez, on est toujours en plein Prague ! Il fait jour, il fait beau, il fait
  chaud, les ouvriers gazouillent et percent des trous dans les murs à deux 
  mètres de toi, le bonheur.\
  Bon du coup je visite en vitesse une église assez mignonne et j'me remets en 
  quête de voiture.

* 16:50\
  Après environ 3h d'attente, parsemées de lecture, de pause clope et autres 
  activités fort intéressantes : une voiture.

* 17:00\
  Et tu sais quoi ? 10 minutes plus tard c'est déjà fini. Court mais intense.
  Moment parfait pour faire une pause bouffe sur l'aire et _encore_ charger le
  matos.

* 18:30\
  Je recommence le stop. Beaucoup de gens pas très enjoués mais après à peine
  15 minutes un mec vraiment motivé. Il m'explique qu'il a un bachelor en
  informatique, qu'il adore voyager. Forcément je suis chaud et j'le suis, mais
  il a l'air garé un peu loin sur l'aire…\
  Normal, c'est un routier ! Il écoute du Gainsbourg, il adore la France et il 
  m'offre de l'internet et des prises. Il est bulgare, à son compte et là il va 
  à Bratislava. Il doit faire une pause obligatoire — lire dodo — à Brno, pile 
  là où je vais. Merci Svetoslav !\
  \ 
  Bon du coup j'ai plein d'anecdotes sur son camion, vu qu'il l'a payé lui même
  et qu'il en est fier. Il fait donc du freelance en Europe, là il baladait une
  machine de compactage de déchets, au départ d'Amsterdam et à destination 
  d'Athènes. Eh beh.\
  \ 
  Du coup les stats : 
    - Camion : 61 000€
    - Remorque : 28 000€
    - Consommation remorque vide : 22L/100km
    - Consommation remorque pleine : 44L/100km
    - Salaire bulgare : 1 500 à 2 000€/mois
    - Salaire freelance : 5 000€/mois
 
  À un moment on s'arrête sur une aire d'autoroute, il veut prendre une douche.
  Sauf qu'une douche c'est payant, donc il a un réchaud avec de l'eau. On fait 
  ainsi une grosse pause avec du café et du shampoing. À ce moment, il me 
  montre ce qu'il transporte et m'explique plein de trucs sur son camion, un 
  mec génial.\
  C'est aussi à ce moment qu'il retrouve un zippo qu'il croyait avoir perdu. Il
  me fait part d'une phrase philosophique : « In life nothing lose, only change 
  owner ».

  ![vroum vroum](images/brno_camion.jpg)

* 22:45\
  Bon bah du coup on arrive à 10km au sud de Brno, sur une grande aire. La 
  douche est apparemment payante mais la caissière me laisse l'utiliser gratos,
  très gentille !

* 00:07\
  J'étais tranquillement assis dans mon QG, au macdo quand ils décident de me 
  virer. Y'en a qui font la fermeture des bars, moi celle des macdo. Mais au 
  moins tout est chargé et j'ai eu le temps de lire un peu !

* 01:00\
  Je pose la tente sous un arbre, pas trop loin du centre commercial. Il y a 
  une piste cyclable juste à côté qui va dans la ville. Ce sera un bon endroit 
  pour commencer demain matin.

\newpage

### 30/06/16

* 10:10\
  Boom, l'heure de se réveiller, apparemment. Petit tour au supermarché pour
  choper quelques victuailles. Je suis donc la piste cyclable qui va en ville, 
  on est juste à côté ! En chemin, je reçois un appel : la direction de ma 
  résidence veut que j'enlève des trucs de ma porte. C'est ballot j'suis pas 
  vraiment à Paris, allez bisous.

* 13:05\
  Du coup si le lecteur attentif a bien suivi, il y avait de la marche pour 
  arriver à Brno. Et quand on combine sport et chaleur il se passe quoi ? Eh 
  oui, ça fait de la bière.

  ![Encore une Pilsner Urquell](images/brno_biere.jpg)

  J'ai rien compris à la carte qu'on m'a filée, du coup j'ai pris ce que je 
  savais commander. C'est ça s'avoir s'adapter.

* 14:10\
  Après avoir passé mon temps à glander comme d'hab, faudrait ptete visiter 
  plus en profondeur. Brno c'est une ville magnifique, je préfère même à 
  Prague ! Taille humaine, on peut aller où on veut à pied et surtout c'est 
  joli hein.

  ![Église avec poney wtf](images/brno.jpg)


* 16:45\
  Allez, Brno c'était bien mais faut bouger, hop hop hop. Début du stop à un 
  feu rouge, a priori le dernier avant l'autoroute d'après ma carte et ce que 
  me disent mes yeux en regardant les panneaux.

* 17:00\
  Gros fun, non pas une voiture qui s'arrête mais la pluie qui se met à tomber.
  Bon moi je m'en fous, j'ai une veste au fond du sac et une protection pour le
  sac. Mais qui c'est qui s'en fout pas ? Le panneau en carton pour faire du 
  stop.\
  Eh oui, là c'est assez triste, il a décidé de s'auto-détruire. Alors autant 
  du carton ça se trouve facilement, autant le feutre vide lui il écrit plus.
  Donc je repars pour le centre ville qui est pas vraiment à côté, en 
  m'arrêtant dans chaque station service sur le chemin. Personne n'a un putain
  de feutre, comment ils font pour vivre ces sauvages ?

* 18:15\
  Putain, après tout ce temps je peux recommencer à tenir fièrement mon panneau 
  avec marqué sûrement un truc dans le genre de _Ostrava_, la ville à la 
  frontière.

* 19:20\
  Hop, départ pour Ostrava, à peu près. Le mec est père de famille, super 
  sympa, il rentre chez lui. Il a aussi fait du stop quand il était jeune, il 
  était allé à Amsterdam. Du coup il m'aime bien !\
  Pendant le trajet il appelle sa femme, il lui raconte sûrement des trucs 
  intéressants mais moi je comprends rien, classique. Il m'explique quand même
  qu'il lui a parlé de moi et qu'ils sont chauds pour m'héberger cette nuit !
  Là c'est la fête, au début j'hésite puis j'me dis pourquoi pas.

* 21:45\
  On arrive chez lui, il a une grande maison paumée en pleine campagne. Très 
  moderne, on s'y sent bien. Il y avait du coup sa femme, ses deux gosses et 
  même ses deux chiens trop mignons. Tout le monde parlait à peu près anglais
  donc on se comprennait.\
  Ils m'ont fait goûter un plat local super bon, aucune idée de ce que c'était.
  Et après on est allé se poser sur la terrasse avec un bon verre de vin de 
  Moravie. Aussi un petit verre d'eau de vie parce que hein.\
  Une douche et au dodo, ça fait du bien de dormir dans un vrai lit !

\newpage

## Pologne

\backgroundsetup{
scale=1,
color=black,
opacity=0.9,
angle=0,
contents={%
  \includegraphics[width=\paperwidth,height=\paperheight]{images/cracovie_header.jpg}
  }%
}

\thispagestyle{empty}
\BgThispage
\clearpage

### 01/07/16

* 06:00\
  Oui oui, il est 6h du mat et on se réveille. Ça fait bizarre hein ? C'est
  tout simplement parce que le mec est agriculteur, il part toujours tôt. Du 
  coup à 6h15 on est déjà parti !\
  Il a prévu de me déposer à un endroit pas trop compliqué pour faire du stop,
  ça n'en reste pas moins un village paumé.

* 07:00\
  Je faisais du stop en marchant tellement j'avais aucun espoir. Puis à un 
  moment un mec à la station service de l'autre côté de la route me fait signe 
  de venir. Bon alors il parle pas un seul mot d'anglais, il a juste vu que 
  j'avais un gros sac et que j'allais vers Ostrava. Du coup il me prend et on 
  essaye de parler, on y arrive à peu près !

* 07:25\
  Il me dépose au niveau d'un gros rond point, près de la bretelle qui va sur 
  l'autoroute. J'aurais vu d'Ostrava que ce rond point mais bon…

* 09:00\
  Alors oui un peu d'attente, mais c'est normal, horaires de boulot… Vous vous
  voyez vous emmener un mec qui fait du stop à 7h du mat pour traverser la 
  frontière ?\
  M'enfin, un polonais s'arrête et m'emmène jusqu'à Katowice. Il était persuadé
  que j'étais polonais vu ma tête. En général les gens vont pas jusqu'à 
  imaginer qu'on est français vu la distance !

* 09:40\
  Allllleeeeez ! On est en Pologne, et en plus à Katowice ! Ici c'est le hub 
  des transports, tout le monde passe par là. Et en plus, y'a un macdo.
  J'ai énormément de trucs à raconter sur cette aire à la con, j'ai rencontré 
  des gens géniaux, d'autres autostopeurs en fait. On a beaucoup échangé, si ça
  t'intéresse vraiment, suffit de me mp et je t'explique autrour d'une bière 
  tkt.

* 13:20\
  Bon alors les mecs sont partis, reste plus que moi, j'attends sagement sur 
  mon banc à la terrasse du domac. Et là je vois une camionnette, immatriculée
  à Cracovie, ça sent bon ça ! Le mec me regarde avec instance en plus, 
  jackpot ? Je vais voir et en effet, il veut bien m'emmener, il attend juste 
  que son collègue resorte du macdo avec le repas.\
  Les deux gus bossent dans la climatisation à Cracovie, et surtout ils parlent
  pas anglais. Alors j'me suis démerdé avec les quelques mots de polonais que 
  je connais et ça a apparemment suffit.

* 15:25\
  On arrive donc à Cracovie après un passage dans leur boîte. Ils ont des 
  sandwichs gratos là-bas donc ils m'en ont filé, super sympa ! Le conducteur 
  dépose son collègue chez lui puis moi assez proche du centre. Franchement
  heureusement que j'arrivais à blablater quelques mots pour savoir oùj'allais 
  être déposé.

* 16:20\
  Maintenant que je suis à Cracovie, j'ai arrêté un peu d'écrire. Vu que j'ai 
  habité là, j'avais pas grand chose à raconter de nouveau…\
  C'est quand même l'heure d'aller au bar. Du coup, direction _Viva la Pinta_ !
  Un des meilleurs bars de la ville, ils brassent eux mêmes et sont très cools.
  J'ai rencontré trois américains qui attendaient l'ouverture du bar, ils ont
  cru que je venais sur recommendation d'un quelconque site internet. Eh non 
  biatch c'est mon ter ter ici. De mémoire, les deux nanas se connaissaient 
  déjà depuis pas mal de temps et le mec a squatté je sais plus quand. Paye ta 
  précision.

  ![Sour beer, très acide eurk](images/cracovie_biere.jpg)

  C'est aussi là que j'ai été très fier de moi, j'ai un bon accent et je parle 
  bien anglais d'après eux. Eh ouais, merci les filles (h).

* 20:30\
  Un seul bar c'est pas suffisant, on se dirige vers le _Bania Luka_, le genre 
  de bar avec la pinte ou le shot à 1€. Ça fait mal.\

  Après ça je rentre chez la pote qui m'héberge et je profite de la ville. Ça 
  fait un an que j'y étais pas retourné, j'ai un peu de visite à faire !

  ![Bout de la rue](images/cracovie_eglise.jpg)

  Par exemple, j'ai habité dans cette rue là, c'était le chemin pour aller au 
  centre ville, sexy.

\newpage

## Autriche

\backgroundsetup{
scale=1,
color=black,
opacity=0.9,
angle=0,
contents={%
  \includegraphics[width=\paperwidth,height=\paperheight]{images/autriche_header.jpg}
  }%
}

\thispagestyle{empty}
\BgThispage
\clearpage

### 04/07/16

* 12:55\
  Retour à la vie réelle, faut repartir en France pour les examens, nique sa 
  mère. J'me pose à une station service en plein Cracovie, flemme de marcher.
  Là un mec fait le plein, un Italien. J'y prête pas trop attention, je fume ma
  clope et j'écoute ma musique. Puis j'me dis que ça serait bien d'essayer.\
  À peine j'approche le gars qu'il me demande où je veux aller. Et c'est parti,
  on embarque pour motherfucking Vienne. On va traverser toute la République 
  Tchèque d'un coup ! On a fait facile 480km, mhmm.

* xx:xx\
  J'ai pas les horaires parce qu'on s'en fout, mais j'ai des photos.
  Y'avait des bouchons, petite pause au calme.

  ![Bouchon posé](images/cracovie_autoroute.jpg)

  Et l'italien là, il adorait s'arrêter partout. Même sur les bandes d'arrêt 
  d'urgence. Il adore les fleurs donc dès qu'il en voit il s'arrête pour les 
  photographier. Il avait même un tournesol dans la voiture qu'il avait piqué 
  dans un champs. Et puis il avait pas une voiture dégueulasse !

  ![Voiture et lac sympas](images/audi.jpg)

* 19:55\
  Par contre on s'est chié niveau aire d'autoroute, on a raté la dernière de la
  bifurcation… Par contre j'vous ai déjà dit qu'il était super sympa ce type,
  il a continué sur l'autoroute qui va en Allemagne juste pour m'arranger. Bon
  par contre y'a pas 50 aires, il me dépose dans un bled paumé en Autriche avec
  un supermarché encore ouvert. Bisous mon gars et merci.\
  \
  Donc là je cherche un endroit à peu près tranquille pour dormir. Genre un 
  petit abri sur le chemin qui va à la carrière, ça va être bien ça, ça va être
  calme le lendemain matin j'suis sûr hein, boloss. En plus le proprio a voulu 
  me jeter le soir, mais vu que j'écoutais du Mano Solo et que j'ai réussi à 
  lui dire que je repartais le lendemain, il m'a laissé tranquille. Apprennez 
  l'allemand que j'vous dis.

\newpage


## Retour

\backgroundsetup{
scale=1,
color=black,
opacity=0.4,
angle=0,
contents={%
  \includegraphics[width=\paperwidth,height=\paperheight]{images/retour_header.jpg}
  }%
}

\thispagestyle{empty}
\BgThispage

\vspace{15cm}

Ici le but n'était plus de visiter, mais de rentrer à la maison. Du coup on 
expédie les voitures et on espère ne pas trop mourir d'ennui.\
Et cette photo a été prise l'hiver dernier mais elle rentre dans le thème donc
osef.

\clearpage

### 05/07/16

* 10:00\
  J'ai même pas noté à quelle heure j'me suis levé tellement j'en avais rien à
  faire apparemment. Toujours est-il que je suis dans un bled paumé, il faut 
  que je bouge un peu pour arriver à l'autoroute.
  Du coup je me pose près de la bretelle et jackpot. Même pas 10 minutes après 
  une meuf s'arrête et m'emmène sur une aire pas trop loin. Vraiment pas loin
  mais ça m'ammène sur l'autoroute donc je suis content.

* 11:30\
  Je recommence sur cette aire plutôt grande et trouve avec un peu de mal une 
  autre nana qui veut bien m'emmener. Elle a l'air de plutôt bien connaître le
  coin donc je lui fais confiance. De toute façon on en a pour 10 minutes de 
  route à peine.

* 11:45\
  Eh oui, 10 minutes de route… Et en plus on est pas sur la bonne autoroute !
  La meuf devait sortir et m'a dit que c'était une aire sympa. Oui sauf que les
  gens qui passent par là, ils vont pas où je veux…\
  J'ai donc cherché en vain des gens et j'me suis résigné à traverser les 
  champs pour revenir du bon côté et repartir sur l'autoroute.
  \ 
  Dans le process j'ai réussi à déchirer mon short en sautant une barrière, 
  rip.

* 15:50\
  J'ai enfin réussi à m'échapper et je commence à faire du stop sur la bretelle
  d'entrée. Je veux aller sur la bonne autoroute, à partir de là ça devrait 
  aller mieux…\
  Les gens s'arrêtent à chaque fois quand je fais une pause clope, ils sont 
  relous et en plus ils vont pas au bon endroit, dommage…

* 17:15\
  Sauf deux mecs ! Ils vont vers Amstetten, à environ 50km, je suis heureux.
  Deux types très sympas, on discute bien, ils parlent anglais, un peu. Pour 
  une fois que j'ai pas à trop m'emmerder avec l'allemand !\
  Bon par contre ça a été super fun, le passager à un moment sort un flingue de
  la boîte à gant en mode ultra pépère. Comme s'il sortait sa mixtape, 
  bluffant. Bon il me dit en allemand un truc du genre « tkt j'ai un permis »
  tout en me montrant une carte de visite.
  \
  Je m'inquiète pas, ils sont cools. Et si ça se trouve c'était ptete même un 
  gofast vu la bagnole qu'ils avaient et le fait qu'ils parlaient de fumer tout
  le temps. Une bonne rencontre. Le trajet dure environ 30 minutes, pas énorme 
  de toute façon.

* 19:00\
  Je recommence ma quête de voiture sur cette aire très classique : 
  restaurants, grands parkings. De toute façon il ne suffit que d'une voiture, 
  pas besoin de charmer tout le monde.

* 19:07\
  Facile, un mec me prend avec son fils. Ils sont suisses et rentrent à Zurich.
  Coup de bol pour moi, Munich c'est sur la route ! Ils roulaient en Jaguar, 
  très bien équipée et propre. Par contre ils parlaient le suisse-allemand. Le
  trajet a été très calme et on a eu du mal à parler…\
  J'ai quand même cru comprendre que le père avait fait du stop quand il était
  jeune et qu'il voulait aider à son tour. Merci papy !

* 21:55\
  Arrivée du coup à environ 20km de Munich. Tente et dodo, y'a encore 380km 
  pour espérer arriver à Strasbourg.

\newpage

### 06/07/16

* 13:00\
  Encore une fois, osef du réveil. Tout ce qu'on veut c'est tracer direction la
  maison. Donc à 13h, on commence le stop, parce qu'a priori on est une quiche
  et on se lève tard !

* 13:50\
  Un gus m'emmène à Stuttgart ! Ça me gêne vraiment pas ce genre d'attente 
  d'une heure si c'est pour parcourir 240km.

* 16:40\
  Pas grand chose d'intéressant aujourd'hui en fait. J'me remets en quête d'une
  voiture, je demande à tout le monde sur l'aire. Au départ une meuf me dit que
  c'est pas elle qui conduit, pourquoi pas. Puis je tombe sur deux mecs, ils 
  sont d'accord. Et c'est qui qu'on revoit à la voiture ? Eh oui c'est la 
  demoiselle d'avant. 

* 19:55\
  J'me suis rendu compte seulement en arrivant à Karlsruhe que c'était un
  couple qui voyageait en covoit.\
  Ils allaient à la gare de Karlsruhe, moi j'allais essayer de planter ma tente
  quelque part. Il était encore tôt mais j'avais envie d'arriver pas trop tard 
  à Strasbourg.\
  Et du coup le classique : la tente à côté du tram derrière le panneau 
  publicitaire. Ça avait l'air bien au début et j'me suis vite rendu compte que
  les arbres avait été mal taillées, c'était plein de petites souches par 
  terre, pas agréable…

\newpage

### 07/07/16 — Strasbourg

* 10:00\
  On charbonne, départ pour Baden-Baden. Seulement à 40km de Karlsruhe, mais on
  est déjà tellement près de la France que c'est bon à prendre.

* 10:15\
  Qu'est-ce que je disais, on est déjà arrivé. Mais du coup petit-dej, j'ai 
  faim. Ce qui est cool, c'est qu'ici des gens parlent français, on peut les 
  aborder plus facilement.

* 13:55\
  C'est au final après 1h30 de démarchage que Mohamed, fier militant pour le 
  FN, me prend pour Strasbourg. On s'arrête acheter des clopes juste avant de 
  passer la frontière. Toute économie est bonne à prendre.\
  Il me dépose près de la gare de Strasbourg vers 15h, pile là où je voulais 
  aller pour rejoindre des potes ! Coucou si vous lisez ce passage :).

* xx:xx\
  Et le 07 Juillet 2016 y'avait quoi d'intéressant ? La demi finale de 
  l'euro2016 ! On va donc se poser dans la rue pour voir les matchs à la télé.
  Et vu que la France a gagné, on était plutôt joyeux.\
  Ce week-end c'est la finale à Paris, il faut que j'arrive à y être. Surtout 
  qu'un pote de ce soir là y sera également.

\newpage

## Paris

\backgroundsetup{
scale=1,
color=black,
opacity=0.4,
angle=0,
contents={%
  \includegraphics[width=\paperwidth,height=\paperheight]{images/paris_header.jpg}
  }%
}

\thispagestyle{empty}
\BgThispage

\clearpage

### 08/07/16

* xx:xx\
  J'ai passé ma journée à Strasbourg, c'était sympa de raconter ma vie après
  ces deux semaines. Apparemment je le fais encore vu que j'écris ce texte.

* 18:00\
  Je me décide enfin à bouger. Aller, j'me pose devant l'autoroute, en plein 
  Strasbourg. C'est ça qui est cool avec cette ville, pas besoin de marcher
  longtemps !\
  Même pas 5 minutes après, un mec m'emmène pour Saverne, la première aire sur 
  l'autoroute. Il bougeait juste avec sa copine et sa sœur. C'est fou comment 
  ça fait du bien de parler la même langue !

* 18:40\
  Allez, on est à Saverne, on se remet en quête d'une voiture. Facile, 10 
  minutes et une âme charitable qui va à Amiens me dit d'embarquer. Je lui fais
  goûter mon tabac Tchèque, il trouve ça dégueulasse. Normal, c'est
  dégueulasse. Mais ça partait d'un bon sentiment hein…

* 21:20\
  On arrive à Reims-Nord. C'est là que nos routes se séparent, il va au nord, 
  oi je continue à l'Ouest pour Paris.\
  J'étais déjà passé en stop sur l'aire de Reims-Sud, on était resté bloqué à 3
  là-bas pendant 8h, l'horreur.\
  Par contre Reims-Nord ça marche bien je vous dit. À peine sorti de la voiture
  je vais demander à un mec immatriculé 75 (Paris) où il va. Il va vers 
  Versailles, sud ouest de Paris, à peu près ce que je veux ! On parle un peu
  politique et autres, il était très content de m'avoir pris qu'il me dit, va
  savoir.

* 22:55\
  On arrive à Charenton, la banlieue la plus proche de chez moi. Il me largue 
  au niveau du métro. Y'a encore des transports donc je peux rentrer 
  tranquillement chez moi, ce qui fait tout bizarre…

\newpage

## Temps d'attente

Un truc qu'on me dit souvent, c'est que le stop ça doit être galère d'attendre
150 ans pour avoir une voiture. Si vous avez lu jusqu'ici vous aurez pu voir 
que non, en fait ça va. Et si vous avez eu la flemme, bah voici un joli 
tableau :

| Jour | Nombre de voitures | Temps d'attente | Moyenne ce jour |
|:----:|-------------------:|----------------:|----------------:|
| 22/06|                   2|             1h05|             0h35|
| 23/06|                   1|             0h45|             0h45|
| 24/06|                   2|             1h05|             0h33|
| 25/06|                   4|             4h10|             1h03|
| 26/06|                   3|             1h00|             0h20|
| 27/06|                   2|             4h39|             2h20|
| 28/06|                   2|             2h30|             1h15|
| 29/06|                   2|             3h05|             1h32|
| 30/06|                   1|             1h00|             1h00|
| 01/07|                   3|             0h30|             0h10|
| 04/07|                   1|             0h00|             0h00|
| 05/07|                   4|             1h42|             0h25|
| 06/07|                   2|             2h10|             1h05|
| 07/07|                   2|             1h35|             0h47|
| 08/07|                   3|             0h15|             0h05|

Moyenne d'attente : 45 minutes, réparties sur 34 voitures. Ça peut paraître 
beaucoup, c'est juste que des fois on a pas de chance…

![Répartition de l'attente](images/attente.jpg)

On voit ici qu'on a pas forcément besoin de beaucoup attendre ! 
On va arrondir à 3800km de trajet, ça fait environ 100km par voiture ! Plutôt 
pas mal comme performance. Surtout avec 45 minutes d'attente de moyenne !

\newpage

# Le petit mot de la fin

Tout a été dit mais si vous voulez un tl;dr, c'est avec plaisir que je vais
écrire un pavé.

Je suis parti à l'arrache, je ne savais pas où j'allais. J'ai rencontré des
gens géniaux. Le pire est de se dire qu'on fait des rencontres éphémères, on 
aimerait continuer à échanger. Je suis encore en contact avec quelques types,
ça fait plaisir. Mais ça permet aussi de vivre dans le moment.

Et c'est ça que j'ai adoré. Si tu veux faire quelquechose, tu le fais ! Pas 
besoin d'attendre l'approbation de quelqu'un, pas besoin d'attendre qu'on te
propose une soirée. La soirée elle se fait pas si tu bouges pas ton cul.

Être livré à soi même, c'est ça mon kiff. L'aventure, ne pas savoir où on va.
Alors souvent on a l'impression qu'on va jamais sortir de ce merdier, mais 
c'est toujours temporaire.

En deux semaines et demies, j'ai beaucoup voyagé. Voici un bref résumé de ce 
que j'ai pu faire. Il faut compter pas mal de km en plus pour tout ce que j'ai
pu marcher et tous les détours à la con. Ça reste impressionant…

![Carte résumé](images/map.png)

Si vous avez des questions à propos de ce voyage, toutes les infos sont dans l'intro :).
